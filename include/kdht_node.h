#ifndef KDHT_NODE_H
#define KDHT_NODE_H

#include <stdlib.h>
#include <stdint.h>

#define DHT_K 20
#define DHT_ID_BYTES 20
#define DHT_BITS DHT_ID_BYTES * 8
#define DHT_ALPHA 3


#define FOR_EACH_DHT_K(x) for (int x = 0; x < DHT_K; x++)
#define FOR_EACH_DHT_ID_BYTE(x) for (int x = 0; x < DHT_ID_BYTES; x++)




/**
 * @brief Predefined size byte array
 * Most significant bit is 0
 *
 */
typedef struct kdht_node_t {
	uint8_t id[DHT_ID_BYTES];
} kdht_node_t;

/**
 * @brief Predefined size string
 *
 */
typedef struct kdht_node_string_t {
	char id[DHT_ID_BYTES * 2 + 1];
} kdht_node_string_t;

/**
 * @brief Generates random bytes for dst
 *
 * @param dst p_dst:...
 */
void kdht_node_random(kdht_node_t *dst);

/**
 * @brief Compares two nodes
 *
 * @param src p_src:...
 * @param dst p_dst:...
 * @return int | 0 on equality, -1 on inequality
 */
int kdht_node_equals(kdht_node_t * src, kdht_node_t * dst);

/**
 * @brief Copies from one node to another
 *
 * @param src p_src:...
 * @param dst p_dst:...
 */
void kdht_node_copy(kdht_node_t* src, kdht_node_t* dst);

/**
 * @brief Encodes hex string into dst from node src
 *
 * @param src p_src:...
 * @param dst p_dst:...
 */
void kdht_node_tostring(kdht_node_t  *src, kdht_node_string_t *dst);

/**
 * @brief Decodes hex from string src into bytes dst
 *
 * @param src p_src:...
 * @param dst p_dst:...
 * @return int | 0 on success, -1 on Decoding error
 */
int kdht_node_parse(kdht_node_string_t *node_str, kdht_node_t*dst);


/**
 * @brief Scans from 0 to DHT_ID_BYTES to find first set bit within that byte
 *
 * @param dist p_dist:...
 * @return uint16_t | total bit position of first set bit
 */
uint16_t kdht_node_most_sig_bit_pos(kdht_node_t  *dist);


/**
 * @brief Populates result with xor resulting in bits equaling the distance.
 *
 * @param left p_left:...
 * @param right p_right:...
 * @param result p_result:...
 */
void kdht_node_calculate_distance(kdht_node_t  *left, kdht_node_t  *right, kdht_node_t  *result);
#endif
