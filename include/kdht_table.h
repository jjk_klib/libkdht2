#ifndef KDHT_TABLE_H
#define KDHT_TABLE_H
#include <stdint.h>

#include <klist.h>

#include <kdht_bucket.h>
#define KDHT_TABLE_REPUBLISH_LOCAL_VALUES_SECONDS 86400
#define KDHT_TABLE_REPLICATE_REMOTE_VALUES_SECONDS 3600
#define KDHT_TABLE_T_ELEMENTS DHT_BITS

typedef struct kdht_value_t {
	kdht_node_t key;
	uint16_t len;
	uint8_t *bytes;
	uint64_t last_replicate_time;
	uint64_t expire_time;
} kdht_value_t;

/*
 * (farthest)0...159 (closest)
 * */
typedef struct kdht_table_t {
	kdht_node_t current_node;
	size_t total_contacts;
	kdht_bucket_t routing_table[KDHT_TABLE_T_ELEMENTS];
	klist_t remote_values;
	klist_t local_values;
} kdht_table_t;


void kdht_table_init_allocs(kdht_table_t *table);
void kdht_table_release_frees(kdht_table_t *table);

uint16_t kdht_table_find_index(kdht_table_t *table, kdht_node_t *dst_node);
uint64_t kdht_table_calculate_value_expire_time(kdht_table_t *table, kdht_contact_t *contact);
void kdht_table_update_contact_allocs_frees(uint64_t current_time, kdht_table_t *table, kdht_contact_t *contact);

kdht_contact_t* kdht_table_find_contact(kdht_table_t *table, kdht_node_t *node);
void kdht_table_find_n_close_nodes_allocs(kdht_table_t *table, kdht_node_t *requestor, kdht_node_t *search, size_t n, klist_t *results);

klist_node_t *kdht_table_get_value(klist_t *values, kdht_node_t *key);
kdht_value_t *kdht_table_insert_value(klist_t *values, kdht_value_t *value);


void kdht_table_release_values_frees(klist_t *values);
#endif
