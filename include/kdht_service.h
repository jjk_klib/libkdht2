#ifndef KDHT_SERVICE_H
#define KDHT_SERVICE_H
#include <stdlib.h>
#include <stdint.h>

#include <kservice.h>
#include <kdht_table.h>
#include <kdht_rpc.h>

#define KDHT_SERVICE_SELECT_TIMEOUT_USEC 1000;
#define KDHT_MESSAGE_MAX_LEN 4096

typedef struct kdht_service_config_t {
	//other things like backlog, timeout, threads whatever.
	long select_timeout_usec;
	long network_timeout_seconds;
	long expire_seconds;
	long refresh_seconds;
	long republish_seconds;
	long replicate_seconds;
} kdht_service_config_t;


typedef struct kdht_service_client_t {
	kservice_client_t client;
	char remote[KSERVICE_MAX_ADDR_LENGTH];
	size_t msg_wait_len;
} kdht_service_client_t;

typedef struct kdht_service_t {
	kdht_rpc_t rpc;
	kdht_service_config_t config; //config.contact.node and config.contact.public_address
	uint64_t last_refresh_time;
	uint64_t last_gc_time;
} kdht_service_t;






void kdht_service_init(kdht_service_t *dht, knet2_ip_handler_t *net);
void kdht_service_release_frees(kdht_service_t *dht);


void kdht_service_loop_iteration(kservice_t *service, void *context);
void kdht_service_handle_io_event(kservice_t *service, kservice_client_t *base_client, knet2_event_t *event);
int kdht_service_handle_io_read(kservice_t *service, kservice_client_t *base_client);
int kdht_service_handle_buffered_message(kdht_service_t  *dht, kdht_service_client_t *client,  uint8_t* buffer, size_t len);


typedef struct kdht_protobuf_message kdht_protobuf_message;

int kdht_service_send_message(kdht_service_t *dht, const char *remote, kdht_protobuf_message *container);
int kdht_service_ping_send_request(kdht_service_t *dht, const char *host);
int kdht_service_iterative_find_node(kdht_service_t *dht, kdht_node_t *target, int bootstrapping, kdht_find_node_t *call);
int kdht_service_find_node_send_request(kdht_service_t * dht, kdht_find_node_t *call, const char *host, kdht_node_string_t *node_str, int bootstrapping, int iterative, RpcCookieIterativeCallType call_type);
int kdht_service_bootstrap_allocs(kdht_service_t *dht, const char **seed_hosts, size_t seed_count);

int kdht_service_refresh_nodes(kdht_service_t *dht, uint32_t SECONDS_THRESHOLD);
int kdht_service_expire_nodes(kdht_service_t *dht, uint32_t SECONDS_THRESHOLD);
int kdht_service_refresh_bucket(kdht_service_t *dht, uint16_t index, kdht_bucket_t *bucket, uint32_t SECONDS_THRESHOLD);
int kdht_service_expire_bucket(kdht_service_t *dht, kdht_bucket_t *bucket, uint32_t SECONDS_THRESHOLD);

int kdht_service_replicate_values(kdht_service_t *dht, klist_t *values_list, uint32_t SECONDS_THRESHOLD);
void kdht_service_iterative_store_value_finished(kdht_rpc_t *rpc, kdht_find_node_t *call);
int kdht_service_iterative_store_value(kdht_service_t *dht, kdht_value_t *value);
int kdht_service_send_store_value(kdht_service_t *dht, char *host, kdht_value_t *value, int iterative);



int kdht_service_send_findvalue(kdht_service_t *dht, char *host, kdht_node_t *target, int iterative, kdht_find_node_t *call);
void kdht_service_iterative_findvalue_finished(kdht_rpc_t *rpc, kdht_find_node_t *call);
int kdht_service_iterative_findvalue(kdht_service_t *dht, kdht_node_t *target);


int kdht_service_gc_outstanding_calls(kdht_service_t *dht, uint32_t SECONDS_THRESHOLD);
#endif
