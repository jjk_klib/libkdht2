#include <arpa/inet.h> //htons only.
#include <string.h>

#include <kdht_log.h>

#include <kdht_service.h>

int kdht_service_gc_outstanding_calls(kdht_service_t *dht, uint32_t SECONDS_THRESHOLD)
{
	klog_msg(DEBUG, "kdht_service_gc_outstanding_calls");
	int i = 0;

	klist_node_t *next = dht->rpc.find_node_calls.head;

	while (next) {
		kdht_find_node_t *call = next->value;
		klist_node_t *tmp = next->next;
		uint64_t timediff = KSERVICE_TIME_DIFF(dht->rpc.service.timestamp, call->initiated_time);
		//TODO: the way this is called, this will always be true; is that intended?
		if (KSERVICE_TIME_SECONDS(timediff) > SECONDS_THRESHOLD) {
			kdht_rpc_execute_andrelease_findnode_frees(&dht->rpc, call);
			i++;
		}

		next = tmp;
	}
	return i;
}

int kdht_service_replicate_values(kdht_service_t *dht, klist_t *values_list, uint32_t SECONDS_THRESHOLD)
{
	klog_msg(DEBUG, "kdht_service_replicate_remote_values");

	int i = 0;
	KLIST_FOR_EACH_NODE(node, values_list) {
		kdht_value_t *value = node->value;
		//need to determine if it has been roughly tReplicate since last replicate.
		uint64_t timediff = KSERVICE_TIME_DIFF(dht->rpc.service.timestamp, value->last_replicate_time);

		if (KSERVICE_TIME_SECONDS(timediff) > SECONDS_THRESHOLD) {
			kdht_service_iterative_store_value(dht, value);
			i++;
		}
	}
	return i;
}


int kdht_service_expire_nodes(kdht_service_t *dht, uint32_t SECONDS_THRESHOLD)
{
	klog_msg(DEBUG, "kdht_service_expire_nodes");
	//when iterating buckets, need to iterate head->tail || timediff > tExpire property.
	for (int i = 0; i < KDHT_TABLE_T_ELEMENTS; i++) {
		kdht_bucket_t *bucket = &dht->rpc.table.routing_table[i];
		kdht_service_expire_bucket(dht, bucket, SECONDS_THRESHOLD);
	}
	return 0;
}


int kdht_service_expire_bucket(kdht_service_t *dht, kdht_bucket_t *bucket, uint32_t SECONDS_THRESHOLD)
{
	klist_node_t *node = bucket->contacts_map.index.list.head;

	while (node) {
		kdht_contact_t *contact = node->value;
		node = node->next;
		kdht_node_string_t buff;
		kdht_node_tostring(&contact->node, &buff);
		uint64_t timediff = KSERVICE_TIME_DIFF(dht->rpc.service.timestamp, contact->last_updated_time);
		if (KSERVICE_TIME_SECONDS(timediff) >= SECONDS_THRESHOLD) {
			klog_msg(ERROR, "expiring %s@%s", buff.id, contact->network_address);
			khashmap_key_t key = { 0 };
			key.data = contact->node.id;
			key.len = sizeof(kdht_node_t);
			kindexedmap_remove_frees(&bucket->contacts_map, &key);
			kdht_contact_release_frees(contact);
		}
	}
	return 0;
}

int kdht_service_refresh_nodes(kdht_service_t *dht, uint32_t SECONDS_THRESHOLD)
{
	klog_msg(DEBUG, "kdht_service_refresh_nodes");
	//when iterating buckets, need to iterate head->tail || timediff > tExpire property.
	for (int i = 0; i < KDHT_TABLE_T_ELEMENTS; i++) {
		kdht_bucket_t *bucket = &dht->rpc.table.routing_table[i];
		kdht_service_refresh_bucket(dht, i, bucket, SECONDS_THRESHOLD);
	}
	return 0;
}

void kdht_service_refresh_bucket_complete(kdht_rpc_t *rpc, kdht_find_node_t *call)
{
	klog_msg(DEBUG, "kdht_service_refresh_bucket finished");
}
int kdht_service_refresh_bucket(kdht_service_t *dht, uint16_t index, kdht_bucket_t *bucket, uint32_t SECONDS_THRESHOLD)
{
	uint64_t timediff = KSERVICE_TIME_DIFF(dht->rpc.service.timestamp, bucket->last_refresh_time);

	if (KSERVICE_TIME_SECONDS(timediff) >  SECONDS_THRESHOLD) {
		//stop this from happening until next period.
		bucket->last_refresh_time = dht->rpc.service.timestamp;

		//the node selects a random number in that range and does a refresh, an iterativeFindNode using that number as key.
		kdht_rpc_findnode_get_or_create_call_allocs(dht->rpc.service.timestamp, &dht->rpc.table, &dht->rpc.contact_information.node, &dht->rpc.find_node_calls,
							    RPC_COOKIE_ITERATIVE_CALL_TYPE__ITERATIVE_FIND_NODE,
							    &dht->rpc.table.current_node,
							    kdht_service_refresh_bucket_complete
							    );

		kdht_node_t target;
		kdht_node_copy(&dht->rpc.table.current_node, &target);
		target.id[index / 8] = rand() % 255;
		kdht_find_node_t *call = kdht_rpc_findnode_get_or_create_call_allocs(dht->rpc.service.timestamp, &dht->rpc.table, &dht->rpc.contact_information.node, &dht->rpc.find_node_calls,
										     RPC_COOKIE_ITERATIVE_CALL_TYPE__ITERATIVE_FIND_NODE,
										     &target,
										     kdht_service_refresh_bucket_complete
										     );
		kdht_service_iterative_find_node(dht, &target, 0, call);
	}
	return 0;
}

void kdht_service_loop_iteration(kservice_t* service, void *context)
{
	kdht_service_t *dht = (kdht_service_t*)service;

	uint64_t timediff;

//      timediff = KSERVICE_TIME_DIFF(dht->rpc.service.timestamp, dht->last_gc_time);
//      if (KSERVICE_TIME_SECONDS(timediff) > dht->config.network_timeout_seconds + 5) {
//              dht->last_gc_time = dht->rpc.service.timestamp;
//              ////TODO: I think this needs to enter this sooner than the outstanding calls expire
//              kdht_service_gc_outstanding_calls(dht, dht->config.network_timeout_seconds);
//              kdht_service_expire_nodes(dht, KDHT_CONTACT_EXPIRE_SECONDS);
//      }

	timediff = KSERVICE_TIME_DIFF(dht->rpc.service.timestamp, dht->last_refresh_time);

	if (dht->config.refresh_seconds != 0 &&
	    KSERVICE_TIME_SECONDS(timediff) > dht->config.refresh_seconds) {
		dht->last_refresh_time = dht->rpc.service.timestamp;
		kdht_service_refresh_nodes(dht, KDHT_CONTACT_REFRESH_SECONDS);

		kdht_service_replicate_values(dht, &dht->rpc.table.local_values, KDHT_TABLE_REPUBLISH_LOCAL_VALUES_SECONDS);
		kdht_service_replicate_values(dht, &dht->rpc.table.remote_values, KDHT_TABLE_REPLICATE_REMOTE_VALUES_SECONDS);
		kdht_service_gc_outstanding_calls(dht, dht->config.network_timeout_seconds);
		kdht_service_expire_nodes(dht, KDHT_CONTACT_EXPIRE_SECONDS);
	}


}


void kdht_service_handle_io_event(kservice_t *service, kservice_client_t *base_client, knet2_event_t *event)
{
	int result = 1;

	if (event->flags & KNET2_EV_ERR) {
		kservice_destroy_client_frees(service, base_client);
		return;
	}
	if (event->flags & KNET2_EV_IN) {
		result = kdht_service_handle_io_read(service, base_client);
	}
	if (result >= 0 && event->flags & KNET2_EV_OUT) {
		knet2_error_t error = { 0 };
		result = kservice_write_client(service, base_client, &error);
		if (result == 0) {
			result = base_client->write_buffers.count > 0;
		}
	}
	if (result <= 0) {
		kservice_destroy_client_frees(service, base_client);
	}
}

int kdht_service_handle_io_read(kservice_t *service, kservice_client_t *base_client)
{
	kdht_service_t  *dht = (kdht_service_t*)service;
	kdht_service_client_t *client = (kdht_service_client_t*)base_client;
	knet2_error_t knet2error;

	uint8_t buffer[4096];
	size_t buflen = sizeof(buffer);

	//part of our network protocol is to send msg len in first 2 bytes, then we don't read unless we have that following.
	if (client->msg_wait_len == 0) {
		//we haven't received 2 byte msg len yet.
		buflen = 2;
	} else if ( client->msg_wait_len >= buflen ) {
		klog_msg(ERROR, "kdht_service_handle_client msg_wait_len(%d) >= buflen(%d)", client->msg_wait_len, buflen);
		return -4;
	}

	int n = dht->rpc.service.network_handler->recv(dht->rpc.service.network_handler, client->client.addr, buffer, buflen, &knet2error);
	if (n < 0) {
		//this will
		klog_msg(ERROR, "kdht_service_handle_client: %s [errno=%d: %s]", knet2error.knet2_errmsg, knet2error.errcode, knet2error.proto_errmsg);
		return -5;
	} else if (client->msg_wait_len == 0) {
		if (n == 2) {
			uint16_t msg_wait_len = ntohs(*(uint16_t*)buffer);
			if (msg_wait_len > KDHT_RPC_MAX_WAIT_LEN) {
				klog_msg(INFO, "kdht_service_handle_client recieved too many bytes at %d", client->msg_wait_len);
				return -7;
			} else {
				client->msg_wait_len = msg_wait_len;
			}
		} else if (n != 0) {
			klog_msg(INFO, "kdht_service_handle_client did not receive 2 bytes but %d", n);
			return -6;
		}
	} else if (n != client->msg_wait_len) {
		klog_msg(INFO, "kdht_service_handle_client did not receive msg_wait_len(%d) but %d", client->msg_wait_len, n);
		return -8;
	} else if (n > 0) {
		//get it ready for another.
		client->msg_wait_len = 0;
		int result = kdht_service_handle_buffered_message(dht, client, buffer, n);
		if (result >= 0) {
			base_client->last_seen_time = dht->rpc.service.timestamp;
		} else {
			return result;
		}
	}
	return 1;//will never close after read, because always outbound or wait for more - unless handle buffered message sees error which is handled above.
}


void kdht_service_init(kdht_service_t *dht, knet2_ip_handler_t *net)
{
	kservice_init(&dht->rpc.service, net, sizeof(kdht_service_client_t), dht->config.network_timeout_seconds);
	kdht_rpc_init(&dht->rpc);

	dht->rpc.service.handle_io_event = kdht_service_handle_io_event;

	kservice_add_loop_complete_callback(&dht->rpc.service, NULL, kdht_service_loop_iteration);

	dht->rpc.service.timestamp = kservice_timestamp();

	dht->config.select_timeout_usec = KDHT_SERVICE_SELECT_TIMEOUT_USEC;
	dht->config.expire_seconds = KDHT_CONTACT_EXPIRE_SECONDS;
	dht->config.refresh_seconds = KDHT_CONTACT_REFRESH_SECONDS;
	dht->config.replicate_seconds = KDHT_TABLE_REPLICATE_REMOTE_VALUES_SECONDS;
	dht->config.republish_seconds = KDHT_TABLE_REPUBLISH_LOCAL_VALUES_SECONDS;

}

void kdht_service_release_frees(kdht_service_t *dht)
{
	kservice_shutdown_frees(&dht->rpc.service);
	kdht_rpc_release_frees(&dht->rpc);
}

int kdht_service_handle_buffered_message(kdht_service_t  *dht, kdht_service_client_t *client,  uint8_t* buffer, size_t len)
{
	klog_msg(DEBUG, "kdht_service_handle_buffered_message: %s", client->remote);

	RpcMessage *message = rpc_message__unpack(NULL, len, buffer);
	int validation = kdht_rpc_validate_message(&dht->rpc, message);
	if (validation) {
		return validation;
	}
	memset(client->remote, 0, sizeof(client->remote));
	strncpy(client->remote, message->header->src->addr, sizeof(client->remote));

	kdht_contact_t *src_contact = kdht_contact_new_allocs((kdht_node_t*)message->header->src->id.data, client->remote);

	kdht_protobuf_message responses[DHT_K];
	memset(responses, 0, sizeof(responses));

	int num_outbound = 0;

	num_outbound = kdht_rpc_handle_message(&dht->rpc, src_contact, message, responses, DHT_K);
	//0 means success without response message

	int result = 0;
	for (int i = 0; i < num_outbound; i++) {
		kdht_service_send_message(dht, responses[i].destination_network_address, &responses[i]);
	}

	rpc_message__free_unpacked(message, NULL);

	kdht_contact_release_frees(src_contact);

	return result;
}

int kdht_service_send_message(kdht_service_t *dht, const char *remote, kdht_protobuf_message *container)
{
	klog_msg(DEBUG, "kdht_service_send_message");
	if (!strcmp(remote, dht->rpc.contact_information.network_address)) {
		klog_msg(ERROR, "attempting to send to our own address");
		return -1;
	}

	if (!container->message.header->is_response) {
		size_t cookie_len = rpc_cookie__get_packed_size(&container->header.decryptedCookie.container);
		if (cookie_len > KDHT_MAX_COOKIE_LEN) {
			klog_msg(ERROR, "cookie_len(%d) would breach proto limit(%d).", cookie_len, KDHT_MAX_COOKIE_LEN);
			return -1;
		} else {
			container->header.container.cookie.len = cookie_len;
			container->header.container.cookie.data = container->header.cookiebuffer;
			rpc_cookie__pack(&container->header.decryptedCookie.container, container->header.cookiebuffer);
		}
	} else {
		klog_msg(DEBUG, "kdht_service_send_message: sending response");
	}

	size_t len = rpc_message__get_packed_size(&container->message) + 2;
	if (len > KDHT_MESSAGE_MAX_LEN) {
		klog_msg(ERROR, "kdht_service_send_message len(%d) is larger than %d", len, KDHT_MESSAGE_MAX_LEN);
		return -1;
	}
	knet2_error_t knet2error;

	kservice_client_t *client = kservice_connect_allocs(&dht->rpc.service, remote, &knet2error);
	if (!client) {
		klog_msg(ERROR, "kdht_service_send_message failed to send to %s: %s [errno=%d: %s]", remote, knet2error.knet2_errmsg, knet2error.errcode, knet2error.proto_errmsg);
		return -3;
	}
	uint8_t buffer[KDHT_MESSAGE_MAX_LEN];
	*(uint16_t*)buffer = htons(len - 2);
	rpc_message__pack(&container->message, buffer + 2);//copy 2 bytes in; first two bytes is length in big endian.
	kservice_queue_write_allocs(&dht->rpc.service, client, (uint8_t*)buffer, len);

	return 0;
}

int kdht_service_ping_send_request(kdht_service_t *dht, const char *host)
{
	klog_msg(DEBUG, "Sending PING to %s", host);
	kdht_protobuf_message init;
	kdht_contact_t contact;
	memset(&contact, 0, sizeof(kdht_contact_t));
	contact.network_address = (char*)host;

	kdht_rpc_init_ping_request(&dht->rpc, &contact, &init);

	return kdht_service_send_message(dht, host, &init);
}

int kdht_service_iterative_find_node(kdht_service_t *dht, kdht_node_t *target, int bootstrapping, kdht_find_node_t *call)
{
	//Find K closest nodes and send find node,  be sure to free the listnodes but not their values.
	klist_t contact_list;

	memset(&contact_list, 0, sizeof(klist_t));
	kdht_table_find_n_close_nodes_allocs(&dht->rpc.table, &dht->rpc.table.current_node, target, DHT_K, &contact_list);
	int sent = 0;
	while (contact_list.head != NULL) {
		klist_node_t *node = contact_list.head;
		klist_remove_node(&contact_list, node);
		kdht_contact_t *contact = node->value;
		kdht_node_string_t current_node_str;

		kdht_node_tostring(target, &current_node_str);
		if (kdht_service_find_node_send_request(dht, call, contact->network_address,  &current_node_str, bootstrapping, 1, call->call_type) > 0) {
			sent++;
		}
		//the contact comes from table and should not be freed.
		//the list nodes are created, and freed
		free(node);
	}


	return sent;
}

void kdht_service_bootstrap_complete(kdht_rpc_t *dht, kdht_find_node_t *call)
{
	klog_msg(DEBUG, "Bootstrap finished!!!!");
}

int kdht_service_bootstrap_allocs(kdht_service_t *dht, const char **seed_hosts, size_t seed_count)
{
	klog_msg(DEBUG, "Initiating bootstrap");

	for (int i = 0; i < seed_count; i++) {
		kdht_contact_t *contact = calloc(sizeof(kdht_contact_t), 1);
		contact->network_address = strdup(seed_hosts[i]);
		kdht_table_update_contact_allocs_frees(dht->rpc.service.timestamp, &dht->rpc.table, contact);
	}

	//TODO: just set this in config along side parsed node.
	kdht_node_string_t current_node_str;
	kdht_node_tostring(&dht->rpc.contact_information.node, &current_node_str);

	kdht_find_node_t *call = kdht_rpc_findnode_get_or_create_call_allocs(dht->rpc.service.timestamp, &dht->rpc.table, &dht->rpc.contact_information.node, &dht->rpc.find_node_calls,
									     RPC_COOKIE_ITERATIVE_CALL_TYPE__ITERATIVE_FIND_NODE,
									     &dht->rpc.contact_information.node,
									     kdht_service_bootstrap_complete);

	int sent = kdht_service_iterative_find_node(dht, &dht->rpc.table.current_node, 1, call);
	//now empty the table of bs entries because of timestamp = 0;
	kdht_service_expire_nodes(dht, 0);


	if (sent == 0) {
		klog_msg(ERROR, "bootstrap failed - not one seed node was able to be sent a message");
		return -1;
	}
	klog_msg(DEBUG, "Bootstrap started with %i sent", sent);
	return sent;
}

int kdht_service_find_node_send_request(kdht_service_t * dht, kdht_find_node_t *call, const char *host, kdht_node_string_t *node_str, int bootstrapping, int iterative, RpcCookieIterativeCallType call_type)
{
	klog_msg(DEBUG, "Sending FIND_NODE to %s", host);
	kdht_protobuf_message init;
	kdht_contact_t contact;
	memset(&contact, 0, sizeof(kdht_contact_t));
	contact.network_address = (char*)host;
	kdht_node_parse(node_str, &contact.node);


	kdht_rpc_init_findnode_request(&dht->rpc, call, &init, &contact, &contact.node, bootstrapping, iterative, call_type);

	return kdht_service_send_message(dht, host, &init);
}


//TODO: why is this rpc?  callback maybe?
void kdht_service_iterative_store_value_finished(kdht_rpc_t *rpc, kdht_find_node_t *call)
{
	kdht_service_t *dht = (kdht_service_t*)rpc;

	klog_msg(DEBUG, "kdht_service_iterative_store_value_finished; now iteratively store in top 20 closest nodes");

	klist_node_t *value_node = kdht_table_get_value(&rpc->table.local_values, &call->target);

	if (!value_node) {
		klog_msg(ERROR, "kdht_service_iterative_store_value_finished; value not found");
		return;
	}
	kdht_value_t *value = value_node->value;

	size_t total = 0;
	KLIST_FOR_EACH_NODE(node, &call->shortlist) {
		kdht_contact_t *contact = node->value;
		kdht_protobuf_message init;

		kdht_rpc_init_storevalue_request(rpc, &init, contact, value, 0);

		if (!kdht_service_send_message(dht, contact->network_address, &init)) {
			total++;
		}
		if (total >= DHT_ALPHA) {
			break;
		}
	}

}

/*
   exec ITERATIVE_STORE_VALUE 6666666666666666666666666666666666666666 ad398c6a94711668d9748bdd89b66ebee7c3b9bb6018803d2d5d945b30dd2d8b415046bbfb7197ee72bb15ed7aaeefd1c93f885c38b18ba0317f020d5b5671c4

 */
int kdht_service_iterative_store_value(kdht_service_t *dht, kdht_value_t *value)
{
	klog_msg(DEBUG, "kdht_service_iterative_store_value");
	kdht_table_insert_value(&dht->rpc.table.local_values, value);

	kdht_find_node_t *call = kdht_rpc_findnode_get_or_create_call_allocs(dht->rpc.service.timestamp, &dht->rpc.table, &dht->rpc.contact_information.node, &dht->rpc.find_node_calls,
									     RPC_COOKIE_ITERATIVE_CALL_TYPE__ITERATIVE_STORE_VALUE,
									     &value->key,
									     kdht_service_iterative_store_value_finished );
	return kdht_service_iterative_find_node(dht, &value->key, 0, call);
}


int kdht_service_send_store_value(kdht_service_t *dht, char *host, kdht_value_t *value, int iterative)
{
	klog_msg(DEBUG, "Sending FIND_NODE to %s", host);
	//TODO: What if it already exists...
	kdht_table_insert_value(&dht->rpc.table.local_values, value);

	kdht_protobuf_message init;
	kdht_contact_t contact;
	memset(&contact, 0, sizeof(kdht_contact_t));
	contact.network_address = host;
	kdht_node_copy(&value->key, &contact.node);

	kdht_rpc_init_storevalue_request(&dht->rpc, &init, &contact, value, iterative);

	return kdht_service_send_message(dht, host, &init);
}


void kdht_service_iterative_findvalue_finished(kdht_rpc_t *rpc, kdht_find_node_t *call)
{
	klog_msg(DEBUG, "kdht_service_iterative_findvalue_finished;");

	//TODO: I think this is actually supposed to be closest in table - except shortlist
	KLIST_FOR_EACH_NODE(node, &call->shortlist) {
		kdht_contact_t *contact = node->value;

		kdht_protobuf_message init;

		kdht_rpc_init_findvalue_request(rpc, call, &init, contact, &call->target);
		kdht_service_send_message((kdht_service_t*)rpc, contact->network_address, &init);
	}

}

int kdht_service_iterative_findvalue(kdht_service_t *dht, kdht_node_t *target)
{
	klog_msg(DEBUG, "kdht_service_iterative_findvalue");

	kdht_find_node_t *call = kdht_rpc_findnode_get_or_create_call_allocs(dht->rpc.service.timestamp, &dht->rpc.table, &dht->rpc.contact_information.node, &dht->rpc.find_node_calls,
									     RPC_COOKIE_ITERATIVE_CALL_TYPE__ITERATIVE_FIND_VALUE,
									     target,
									     kdht_service_iterative_findvalue_finished);
	kdht_value_t *value = calloc(1, sizeof(kdht_value_t));
	kdht_node_copy(target, &value->key);
	khashmap_key_t key = { 0 };
	key.data = value->key.id;
	key.len = DHT_ID_BYTES;
	//data will get set on successful response.
	kindexedmap_put_allocs(&dht->rpc.outstanding_findvalues, &key, value);

	return kdht_service_iterative_find_node(dht, target, 0, call);
}

int kdht_service_send_findvalue(kdht_service_t *dht, char *host, kdht_node_t *target, int iterative, kdht_find_node_t *call)
{
	klog_msg(DEBUG, "kdht_service_send_findvalue to %s", host);

	kdht_protobuf_message init;
	kdht_contact_t contact;
	memset(&contact, 0, sizeof(kdht_contact_t));
	contact.network_address = host;
	kdht_node_copy(target, &contact.node);

	kdht_rpc_init_findvalue_request(&dht->rpc, call,
					&init, &contact, target);

	return kdht_service_send_message(dht, host, &init);
}
