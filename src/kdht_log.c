#include <kdht_log.h>
#include <stdio.h>
#include <stdarg.h>

static const char *LOG_LEVEL_STR[4] = {
	"DEBUG", "INFO", "WARN", "ERROR"
};


void __log_msg(klog_level level, const char *message, va_list args)
{
	if (level >= sizeof(char*) / sizeof(level)) {
		level = ERROR;
	}
	const char *level_str = LOG_LEVEL_STR[level];
	if (message != 0) {
		printf("%s: ", level_str);
		vfprintf(stdout, message, args);
		printf("\r\n");
	}
}

void klog_msg(klog_level level, const char *message, ...)
{
	va_list args;

	va_start( args, message );
	__log_msg(level, message, args);
	va_end( args );
}

