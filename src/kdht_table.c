#include <stddef.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <kdht_log.h>

#include <kdht_table.h>

void kdht_table_init_allocs(kdht_table_t *table)
{
	for (int i = 0; i < KDHT_TABLE_T_ELEMENTS; i++) {
		kdht_bucket_init_allocs(&table->routing_table[i]);
	}
}


void kdht_table_release_values_frees(klist_t *values)
{
	while (values->head) {
		klist_node_t *this = values->head;
		klist_remove_node(values, this);
		kdht_value_t *value = this->value;
		free(value->bytes);
		free(value);
		free(this);
	}
}

void kdht_table_release_frees(kdht_table_t *table)
{
	for (int i = 0; i < KDHT_TABLE_T_ELEMENTS; i++) {
		kdht_bucket_release_frees(&table->routing_table[i]);
	}
	kdht_table_release_values_frees(&table->remote_values);
	kdht_table_release_values_frees(&table->local_values);
}

kdht_contact_t* kdht_table_find_contact(kdht_table_t *table, kdht_node_t *node)
{
	kdht_node_string_t nodestr;

	kdht_node_tostring(node, &nodestr);
	klog_msg(DEBUG, "kdht_table_get_node %s", nodestr.id);
	unsigned short index = kdht_table_find_index(table, node);
	kdht_contact_t *nl = kdht_bucket_get_contact(&table->routing_table[index], node);
	if (nl) {
		klog_msg(DEBUG, "kdht_table_get_node found node %s in table[%d]", nodestr.id, index);
		return nl;
	} else {
		klog_msg(DEBUG, "kdht_table_find_contact: node not found");
		return NULL;
	}
}


uint16_t kdht_table_find_index(kdht_table_t *table, kdht_node_t *dst_node)
{
	kdht_node_t dist;
	kdht_node_t *src_node = &table->current_node;

	kdht_node_calculate_distance(src_node, dst_node, &dist);
	return kdht_node_most_sig_bit_pos(&dist);
}


uint64_t kdht_table_calculate_value_expire_time(kdht_table_t *table, kdht_contact_t *contact)
{
	/*
	 *  C=( table index of key) * table[0 through index-1].bucket.count + index;
	        24 hours if C > k
	        else 24 *exp(k/C)
	 */

	size_t bucket_index = kdht_table_find_index(table, &contact->node);
	size_t bucket_count_low = 0;

	if (bucket_index > 0) {
		bucket_count_low = table->routing_table[bucket_index - 1].contacts_map.index.list.count;
	}
	int C = table->routing_table[bucket_index].contacts_map.index.list.count + bucket_count_low;
	//TODO: Make this a parameter passed to function, it can be stored in kdht_service_config_t and passed through to kdht_table_update_contact_allocs_frees on in.
	if (C == 0 || C > DHT_K) {
		return 60 * 60 * 24;
	} else {
		return 24 * exp(DHT_K / C);
	}
}

void kdht_table_update_contact_allocs_frees(uint64_t current_time, kdht_table_t *table, kdht_contact_t *contact)
{
	contact->last_updated_time = current_time;

	kdht_node_string_t nodestr;

	memset(&nodestr, 0, sizeof(nodestr));
	kdht_node_tostring(&contact->node, &nodestr);
	klog_msg(DEBUG, "kdht_table_update_contact_allocs_frees %s@%s with expire %d", nodestr.id, contact->network_address, contact->last_updated_time);

	unsigned short index = kdht_table_find_index(table, &contact->node);

	table->total_contacts -= table->routing_table[index].contacts_map.index.list.count;
	kdht_bucket_insert_contact_allocs(&table->routing_table[index], contact);
	table->total_contacts += table->routing_table[index].contacts_map.index.list.count;

}

void kdht_table_find_n_close_nodes_allocs(kdht_table_t *table, kdht_node_t *requestor, kdht_node_t *search, size_t n,  klist_t *results)
{
	uint16_t center = kdht_table_find_index(table, search);

	for (int i = center; i < KDHT_TABLE_T_ELEMENTS && results->count < n; i++) {
		kdht_bucket_t *bucket = &table->routing_table[i];
		kdht_bucket_collect_k_nodes_allocs(bucket, requestor, &table->current_node, results);
	}
	for (int i = center - 1; i >= 0 && results->count < n; i--) {
		kdht_bucket_t *bucket = &table->routing_table[i];
		kdht_bucket_collect_k_nodes_allocs(bucket, requestor, &table->current_node, results);
	}
}

klist_node_t *kdht_table_get_value(klist_t *values, kdht_node_t *key)
{
	KLIST_FOR_EACH_NODE(node, values) {
		kdht_value_t *value = node->value;

		if (!kdht_node_equals(key, &value->key)) {
			return node;
		}
	}
	return NULL;
}

kdht_value_t *kdht_table_insert_value(klist_t *values, kdht_value_t *value)
{
	klist_node_t *found = kdht_table_get_value(values, &value->key);
	kdht_value_t *retval = NULL;

	if (found) {
		if (found->value == value) {
			return NULL;
		}
		retval = found->value;
		klist_remove_node(values, found);
		free(found);
	}

	klist_insert_value_allocs(values, NULL, value);
	return retval;
}






























