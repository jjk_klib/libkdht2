#include <kdht_rpc_findnode.h>
#include <string.h>

int kdht_rpc_findnode_shortlist_contains(kdht_find_node_t *call, kdht_contact_t *target)
{
	KLIST_FOR_EACH_NODE(node, (&call->shortlist)) {
		kdht_contact_t *node_value = node->value;

		if (!kdht_contact_equals(node_value, target)) {
			return 1;
		}
	}
	return 0;
}

int kdht_rpc_findnode_distancesortedlist_add_allocs(klist_t *list, kdht_node_t *target, kdht_contact_t *new_contact)
{
	klist_node_t *insert_before = NULL;

	kdht_node_t distance;

	kdht_node_calculate_distance(target, &new_contact->node, &distance);
	uint16_t new_distance = kdht_node_most_sig_bit_pos(&distance);


	KLIST_FOR_EACH_NODE(node, list) {
		kdht_contact_t *contact = node->value;

		memset(&distance, 0, sizeof(kdht_node_t));
		kdht_node_calculate_distance(target, &contact->node, &distance);
		uint16_t current_node_distance = kdht_node_most_sig_bit_pos(&distance);

		int compare = current_node_distance - new_distance;
		//if current_node_distance most sig is less than most sig of new distance, it's farther away.
		//since it's farther, insert here.'
		if (compare <= 0) {
			insert_before = node;
			break;
		}
	}

	klist_insert_node(list, insert_before, klist_node_new_allocs(new_contact));

	if (list->head != NULL && list->head->value == new_contact) {
		return new_distance;
	}
	return -1;
}

void kdht_rpc_findnode_shortlist_add_allocs(kdht_find_node_t *call, kdht_contact_t *new_contact)
{
	int new_distance = kdht_rpc_findnode_distancesortedlist_add_allocs(&call->shortlist, &call->target, new_contact);

	if (new_distance != -1) {
		call->closest_most_sig = new_distance;
	}
}

//TODO: may need to have a way to add multiple subscribers
kdht_find_node_t* kdht_rpc_findnode_get_call( klist_t* find_node_calls, RpcCookieIterativeCallType call_type, kdht_node_t* target )
{
	KLIST_FOR_EACH_NODE(node, find_node_calls ) {
		kdht_find_node_t *node_value = node->value;

		if (node_value->call_type == call_type && !kdht_node_equals(&node_value->target, target)) {
			return node_value;
		}
	}
	return NULL;
}

kdht_find_node_t *kdht_rpc_findnode_get_or_create_call_allocs(uint64_t current_time, kdht_table_t *table, kdht_node_t *requestor, klist_t *find_node_calls, RpcCookieIterativeCallType call_type, kdht_node_t *target, kdht_rpc_find_node_call_complete on_complete)
{
	kdht_find_node_t *call = kdht_rpc_findnode_get_call(find_node_calls, call_type, target);

	if (call == NULL) {
		call = malloc(sizeof(kdht_find_node_t));
		memset(call, 0, sizeof(kdht_find_node_t));
		kdht_node_copy(target, &call->target);
		call->call_type = call_type;
		call->fn_complete = on_complete;
		call->initiated_time = current_time;
		klist_insert_value_allocs(find_node_calls, NULL, call);
		//populate shortlist with closest contacts.
		klist_t nnd_list;

		memset(&nnd_list, 0, sizeof(klist_t));
		kdht_table_find_n_close_nodes_allocs(table, requestor, target, DHT_ALPHA, &nnd_list);
		KLIST_FOR_EACH_NODE(node, &nnd_list) {
			kdht_contact_t *src_contact = (kdht_contact_t*)node->value;
			kdht_contact_t *new_contact = kdht_contact_new_allocs(&src_contact->node, src_contact->network_address);

			new_contact->last_updated_time = src_contact->last_updated_time;
			kdht_rpc_findnode_shortlist_add_allocs(call, new_contact);
		}
		klist_release_frees(&nnd_list);
	}
	return call;
}

void kdht_findnode_call_release_frees(kdht_find_node_t *call)
{
	while (call->outstanding_contacts.head) {
		klist_node_t *outstanding_node = call->outstanding_contacts.head;
		klist_remove_node(&call->outstanding_contacts, outstanding_node);
		free(outstanding_node->value);
		free(outstanding_node);
	}
	while (call->shortlist.head) {
		klist_node_t *shortlist_node = call->shortlist.head;
		klist_remove_node(&call->shortlist, shortlist_node);
		kdht_contact_t *contact = shortlist_node->value;
		kdht_contact_release_frees(contact);
		free(shortlist_node);
	}
}
