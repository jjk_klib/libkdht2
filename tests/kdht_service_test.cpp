#include "./tests.h"


extern "C" {
#include <knet2_epoll.h>
#include <kdht_service.h>
}

TEST(kdht_service, kdht_service_gc_outstanding_calls) {

}

TEST(kdht_service, kdht_service_replicate_values) {

}

TEST(kdht_service, kdht_service_send_findvalue){

}
TEST(kdht_service, kdht_service_iterative_findvalue_finished){

}
TEST(kdht_service, kdht_service_iterative_findvalue){

}

TEST(kdht_service, kdht_service_iterative_find_node){

}
TEST(kdht_service, kdht_service_iterative_store_value){

}
TEST(kdht_service, kdht_service_iterative_find_value){

}
TEST(kdht_service, kdht_service_iterative_store_value_finished) {

}

TEST(kdht_service, kdht_service_init){
	const char *host_ip = "127.0.0.1:60004";
	kdht_service_t dht = {};

	kdht_node_random(&dht.rpc.contact_information.node);
	dht.rpc.contact_information.network_address = (char*)host_ip;

	knet2_error_t error = { 0 };
	knet2_epoll_handler_t tcp = { 0 };
	knet2_epoll_handler_init_allocs(&tcp, 1000, &error);

	kdht_service_init(&dht, (knet2_ip_handler_t*)&tcp);
	ASSERT_EQ(dht.rpc.service.should_run, 0);
	ASSERT_EQ(dht.rpc.service.network_handler, (knet2_ip_handler_t*)&tcp);
	kdht_service_release_frees(&dht);
}

TEST(kdht_service, kdht_service_expire_nodes) {
	ASSERT_EQ(0, 0);
}
