#include "./tests.h"

extern "C" {
#include <kdht_node.h>
#include <kdht_bucket.h>
#include <string.h>
}

#define NULL_CONTACT ((kdht_contact_t*)NULL)


TEST(kdht_contact, kdht_contact_find_contact_list_node) {
	ASSERT_EQ(0, 0);
}
TEST(kdht_bucket, kdht_table_find_k_close_nodes_allocs){
	kdht_bucket_t bucket = {};

	kdht_bucket_init_allocs(&bucket);
	kdht_node_t this_node = {};


	this_node.id[1] = 1;
	kdht_node_t requestor;
	memset(&requestor, 0, sizeof(kdht_node_t));
	requestor.id[2] = 2;

	kdht_contact_t *contacts[DHT_K] = { 0 };

	FOR_EACH_DHT_K(i) {
		contacts[i] = (kdht_contact_t*)calloc(1, sizeof(kdht_contact_t));
		contacts[i]->node.id[i] = i;
		kdht_bucket_insert_contact_allocs(&bucket, contacts[i]);
	}



	klist_t nnd = {};

	int n = kdht_bucket_collect_k_nodes_allocs(&bucket, &this_node, &requestor, &nnd);
	ASSERT_EQ(DHT_K - 1, n);

	kdht_bucket_release_frees(&bucket);
	test_free_list(&nnd, 0);
}

TEST(kdht, kdht_bucket_get_contact){
	kdht_contact_t *a = (kdht_contact_t*)calloc(1, sizeof(kdht_contact_t));
	kdht_contact_t *b = (kdht_contact_t*)calloc(1, sizeof(kdht_contact_t));
	kdht_contact_t *c = (kdht_contact_t*)calloc(1, sizeof(kdht_contact_t));

	kdht_node_t node = {};
	kdht_bucket_t bucket = {};

	kdht_bucket_init_allocs(&bucket);

	a->node.id[4] = 1;
	a->node.id[5] = 1;

	b->node.id[4] = 1;
	b->node.id[5] = 1;
	b->node.id[6] = 1;

	c->node.id[4] = 1;
	c->node.id[6] = 1;

	node.id[4] = 1;
	node.id[6] = 1;

	khashmap_key_t key = { 0 };
	key.len = sizeof(kdht_node_t);

	key.data = a->node.id;
	kindexedmap_put_allocs(&bucket.contacts_map, &key, a);
	key.data = b->node.id;
	kindexedmap_put_allocs(&bucket.contacts_map, &key, b);
	key.data = c->node.id;
	kindexedmap_put_allocs(&bucket.contacts_map, &key, c);



	kdht_contact_t *found = kdht_bucket_get_contact(&bucket, &node);

	ASSERT_NE(NULL_CONTACT, found);
	ASSERT_EQ(c, found);

	node.id[7] = 1;
	found = kdht_bucket_get_contact(&bucket, &node);
	ASSERT_EQ(NULL_CONTACT, found);

	kdht_bucket_release_frees(&bucket);
}



TEST(kdht_bucket, kdht_bucket_insert_contact_allocs__doespop){
	kdht_bucket_t bucket = {};

	kdht_bucket_init_allocs(&bucket);
	kdht_contact_t *contacts[DHT_K + 1] = { 0 };

	for (int i = 0; i < DHT_K + 1; i++) {
		contacts[i] = (kdht_contact_t*)calloc(1, sizeof(kdht_contact_t));
		contacts[i]->node.id[0] = i + 1;
		kdht_bucket_insert_contact_allocs(&bucket, contacts[i]);
	}
	ASSERT_EQ(DHT_K, bucket.contacts_map.index.list.count);
	kdht_bucket_release_frees(&bucket);
}

TEST(kdht_bucket, kdht_bucket_insert_contact_allocs){
	kdht_contact_t *a = (kdht_contact_t*)calloc(1, sizeof(kdht_contact_t));
	kdht_contact_t *b = (kdht_contact_t*)calloc(1, sizeof(kdht_contact_t));
	kdht_bucket_t bucket = {};

	kdht_bucket_init_allocs(&bucket);


	//insert to empty list
	a->node.id[4] = 1;
	a->node.id[5] = 1;
	a->last_updated_time = 1;
	kdht_bucket_insert_contact_allocs(&bucket, a);
	ASSERT_NE((klist_node_t*)NULL, bucket.contacts_map.index.list.head);
	ASSERT_EQ(a, (kdht_contact_t*)bucket.contacts_map.index.list.head->value);

	//insert second
	b->node.id[4] = 1;
	b->node.id[5] = 1;
	b->node.id[6] = 1;
	b->last_updated_time = 2;
	kdht_bucket_insert_contact_allocs(&bucket, b);
	ASSERT_EQ(b, (kdht_contact_t*)bucket.contacts_map.index.list.tail->value);

	kdht_bucket_release_frees(&bucket);
}
